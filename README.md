# Go Parser
## **Select DB engine**  

Specify "DB_PROVIDER" parameter in .env or .env.dev. Only "postgresql" and "mongodb" are supported.  
Depending on the specified parameter, the appropriate container will be started:

docker-compose.yml:
``` 
...
  db:
    container_name: ${APP_NAME}-${DB_PROVIDER}
    extends:
      file: ./docker-compose.${DB_PROVIDER}.yml
      service: ${DB_PROVIDER}
...
```
docker-compose.mongodb.yml:
```
...
services:
  mongodb:
    image: mongo:latest
    hostname: ${DB_MONGO_ENDPOINT}
...
```
docker-compose.postgresql.yml:
```
...
services:
  postgresql:
    image: postgres:14.4-alpine
    hostname: ${DB_PG_ENDPOINT}
...
```

## **Start service**  
in dev environment:
```
docker-compose -f docker-compose.dev.yml --env-file=.env.dev up -d
go run ./api/main.go
```
and in stage environment:
```
docker-compose up -d
```
and browse: http://localhost:8080/  
API documentation: http://localhost:8080/swagger

dev:
adminer: http://localhost:8880
pgadmin: http://localhost:8888