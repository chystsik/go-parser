package config

import (
	"fmt"
	"os"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
)

type (
	AppConfig struct {
		Http
		Service
		Parser
		Database
	}

	Http struct {
		Host string `env:"VIRTUAL_HOST"`
		Port uint   `env:"API_PORT,required"`
	}

	Service struct {
		PublicFolder      string `env:"PUBLIC_FOLDER,required"`
		SwaggerUiFilePath string `env:"SWAGGERUI_PATH,required"`
		Loglevel          uint   `env:"LOG_LEVEL,required"`
	}

	Parser struct {
		SeleniumHost     string `env:"SELENIUM_ENDPOINT,required"`
		SeleniumPort     uint   `env:"SELENIUM_PORT,required"`
		VacancySiteUrl   string `env:"VACANCIES_SITE_URL,required"`
		VacancySearchUrl string `env:"VACANCIES_SEARCH_PAGE,required"`
		VacanciesPerPage uint   `env:"VACANCIES_PER_PAGE,required"`
		Selectors
	}

	Selectors struct {
		VacancyCount string `env:"SELECTOR_SEARCH_TOTAL,required"`
		OnPageLinks  string `env:"SELECTOR_SEARCH_PAGE_LINKS,required"`
	}

	Database struct {
		Provider string `env:"DB_PROVIDER,required"`
		Mongodb
		Postgresql
	}

	Mongodb struct {
		Host     string `env:"DB_MONGO_ENDPOINT"`
		Port     uint   `env:"DB_MONGO_PORT"`
		User     string `env:"DB_MONGO_USER"`
		Password string `env:"DB_MONGO_PASSWORD"`
	}

	Postgresql struct {
		Host     string `env:"DB_PG_ENDPOINT"`
		Port     uint   `env:"DB_PG_PORT"`
		User     string `env:"DB_PG_USER"`
		Password string `env:"DB_PG_PASSWORD"`
		DbName   string `env:"DB_PG_DBNAME"`
		Schema   string `env:"DB_PG_SCHEMA"`
	}
)

const (
	errDecode = "unable to decode app config into struct, %v"
)

func New() (*AppConfig, error) {
	cfg := &AppConfig{}

	if os.Getenv("ENVIRONMENT") == "" {
		err := godotenv.Load(".env.dev")
		if err != nil {
			return nil, fmt.Errorf(errDecode, err)
		}
	}

	err := env.Parse(cfg)
	if err != nil {
		return nil, fmt.Errorf(errDecode, err)
	}

	return cfg, nil
}
