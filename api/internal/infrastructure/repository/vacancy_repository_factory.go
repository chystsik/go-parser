package repository

import (
	"fmt"

	"go-parser/api/config"
	"go-parser/api/internal/infrastructure/repository/mongodb"
	"go-parser/api/internal/infrastructure/repository/postgresql"
	"go-parser/api/internal/service"

	"github.com/sirupsen/logrus"
)

const (
	errWrognProvider = "DB provider %s is not allowed"
)

func NewVacancyRepository(cfg config.Database, logger logrus.FieldLogger) (service.VacancyRepository, error) {
	switch cfg.Provider {
	case "mongodb":
		return mongodb.NewVacancyRepository(cfg.Mongodb, logger), nil
	case "postgresql":
		return postgresql.NewVacancyRepository(cfg.Postgresql, logger), nil
	default:
		return nil, fmt.Errorf(errWrognProvider, cfg.Provider)
	}
}
