package models

// swagger:model Vacancy
type Vacancy struct {
	Id             int           `json:"id"`
	Posted         string        `json:"datePosted"`
	Expire         string        `json:"validThrough"`
	EmploymentType string        `json:"employmentType"`
	Location       []JobLocation `json:"jobLocation,omitempty"`
	LocationType   string        `json:"jobLocationType"`
	Company        Company       `json:"hiringOrganization"`
	Salary         `json:"baseSalary"`
	Info
}

type JobLocation struct {
	Address string `json:"address,omitempty"`
}

type Salary struct {
	Currency    string `json:"currency"`
	SalaryValue `json:"value"`
}

type SalaryValue struct {
	Min float64 `json:"minValue"`
	Max float64 `json:"maxValue"`
}

type Info struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

type Company struct {
	Name    string `json:"name"`
	WebSite string `json:"sameAs"`
}
