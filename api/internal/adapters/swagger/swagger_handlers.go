package swagger

import (
	"html/template"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

type SwaggerUIHandler interface {
	SwaggerUI(w http.ResponseWriter, r *http.Request)
}

type swaggerUiHandler struct {
	swaggerTemplate string
	logger          logrus.FieldLogger
}

func NewSwaggerUIHandler(tmpl string, logger logrus.FieldLogger) SwaggerUIHandler {
	return &swaggerUiHandler{
		swaggerTemplate: tmpl,
		logger:          logger,
	}
}

func (su *swaggerUiHandler) SwaggerUI(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles(su.swaggerTemplate)
	if err != nil {
		su.logger.Error(err)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		su.logger.Error(err)
	}
}
