package handlers

type SearchQuery struct {
	Query string `json:"query"`
}

// swagger:parameters SearchVacanciesRequest
type SearchVacanciesRequest struct {
	// Ключевое слово для вакансии
	//
	// in:body
	// required:true
	Body SearchQuery
}

type DeleteQuery struct {
	Id int `json:"id"`
}

// swagger:parameters DeleteVacancyRequest
type DeleteVacancyRequest struct {
	// Id вакансии
	//
	// in:body
	// required:true
	Body DeleteQuery
}

type GetQuery struct {
	Id int `json:"id"`
}

// swagger:parameters GetVacancyRequest
type GetVacancyRequest struct {
	// Id вакансии
	//
	// in:body
	// required:true
	Body GetQuery
}
