package main

import (
	"log"

	"go-parser/api/config"
	"go-parser/api/internal/app"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	app.Run(cfg)
}
